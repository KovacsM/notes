# Jegyzetek

A jegyzetek markdown és PDF formátumban is megtalálhatóak.

# Hibás Információk

Ha hibát talász csinálj egy Issue Ticketet és javítom, mégjobb ha te javítod és csinálsz egy pull requestet.

# License

Bővebben információkat a [LICENSE](https://gitlab.com/KovacsM/notes/-/blob/main/LICENSE) oldalon talál.

Ha a [LICENSE](https://gitlab.com/KovacsM/notes/-/blob/main/LICENSE) oldalon található információ és a [creativecommons.org](http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1) oldalon található információk között eltérés lenne, **a [creativecommons.org](http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1) oldalon található információk a dontőek, aktuálisak.**

<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><span property="dct:title">The university notes</span> by <span property="cc:attributionName">Kovács Marcell</span> is marked with <a href="http://creativecommons.org/publicdomain/zero/1.0?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0 Universal<br><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1"></a></p>
