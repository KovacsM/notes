# Definíciók

## 1. Peano Axiómák

1. $$0\in \N$$
2. $$\forall x\in\N,S(n)\in\N$$
3. $$\nexists x: S(x)=0$$
4. $$x=y \Leftrightarrow S(x)=S(y)$$
5. $$\forall K\subseteq\N, (0\in K)\land[(x\in K)\Rightarrow(S(x)\in K)]\Rightarrow K=\N$$

## 2. Komplex számok n. gyökének meghatározása; Áttérés algebrai alakról trigonomettrikus alakra

### 2.1 Áttérés algebrai alakról trigonomettrikus alakra

$$z_1=r*(\cos(\theta)+i*\sin(\theta))$$
$$ r=\sqrt{Im(z)^2 + Re(z)^2}$$
$$\theta = \arctan(\frac{Im(z)}{Re(z)})$$

### 2.2 Komplex számok n. gyökének meghatározása

#### 2.2.1 Alapfogalmak

*z* n. gyöke
: azok a számok, amelyeknek az $n.$ hatványa, egyenlő $z$-el

*n.* egységgyök
: azok a számok, amelyeknek az n. hatványa egyenlő 1-el.

Példa
: Az egy egységgyökei: 1, (-1)

#### 2.2.2 Megoldás menete

* Szigorúan trigonomettrikus alak!

$$z_1=r_1*(\cos(\theta)+i*\sin(\theta))$$

* n. hatványt keresünk vagyis legyen igaz:

$$ \sqrt[n]z_1= z_2$$

* emeljük a két oldalt az n. gyökre és írjuk át trigonomettrikus alakba

$$r_2^n(\cos(n*\varphi)+i*\sin(n*\varphi) = r_1*(\cos(\theta)+i*\sin(\theta))$$

* Kezdjük először az $r_2$ kiszámolásával 

$$ r_2^n=r_1$$
$$ r_2 = \sqrt[n]r_1$$

* A $\varphi$ szögre több értéket is kaphatunk, ez a $\sin, \cos$ függvények periodicitásából következik

$$ \varphi = \frac{\theta+2k\pi}{n}, k\in\Z$$


## 3. Valós számsorozat definíciója
*legalább 3 nevezetes sorozat felsorolása és rövid jellemzésük*

Valós számsorozatok
: Pozitív egész számok halmazán értelmezett függvény
: $f:\N^+\rightarrow\R$

### 3.1 Fibonacci sorozat

$$f_1=1, f_2=1, f_3=f_1+f_2$$

* Nem korlátos (alulról igen, felülről nem)
* Szigorúan monoton nő
* $$\lim_{x\to\infin}f_n=+\infin$$

<!--TODO még 2 sorozat ide!-->

## 4. Valós számsorozatok határértéke
*Minden típus esetén*

### 4.1 A sorozat nő

$$d>0; q>1$$

Ebben az esetben a sorozatnak csak alsó korlátja van ($a_1$), felülről nem korlátos. Szigorúan monoton nő, divergens. Pozitív végtelenben a határértéke pozitív végtelen.
### 4.2 A sorozat konstans

$$d=0;q=1$$

Alulról és felülről is korlátos. A konstans sorozat monoton. Konvergens, és határértéke a sorozat tagja(i).
### 4.3 A sorozat csökken

$$d<0;q<1$$

A sorozat felülről korlátos, felső korlátja: $a_1$. Alulról nem korlátos, Szigorúan monoton csökken. Alsó korlátja a $-\infin$

## 5. Függvények fogalma, értelmezési tartománya értékkészlete

### 5.1 Függvény fogalma

Olyan rendezett számhármas: $f=(A,B,\rho)$, ahol A, és B egy-egy halmaz $\rho$ pedig olyan $\rho \subseteq A \times B$ reláció hogy, minden $x\in A$ legfeljebb egyetlen olyan $y\in B$ létezik amelyre $x\rho y$ teljesül.

Ekkor az $A$ halmazt alaphalmaznak nevezük, a $B$ halmazt pedig képhalmaznak.


### 5.2 Értelmezési tartomány

$f$ értelmezési tartományán a $\rho$ értelmezési tartományát értik.

### 5.3 Értékkészlet

$f$ értékkeszletén a $\rho$ értékkészletét értik.

## 6. Egyváltozós valós-valós függvények határétéke
*Minden típus esetén*

<!-- TODO ezt is megcsinálni -->

## 7. Egyváltozós valós-valós függvények folytonossága

### 7.1 Függvöny folytonossága egy adott pontban

* Fontos előfeltétele a folytonosságnak: az $f(x)$ függvény legyen értelmezve az $x_0$ pontban és annak környezetében
* Cauchy-féle definíció:
$$\forall \epsilon>0, \exists\delta>0, 0<|x-x_0|<\delta, |f(x)-f(x_0)|<\epsilon $$
* Heine-féle definíció
$$ \forall x_n\rightarrow x_0, f(x_n)\rightarrow f(x_0)$$

### 7.2 Függvény folytonossága egy adott intervallumra

Egy függvény akkor folytonos egy adott intervallumon, ha annak minden pontján folytonos.

## 8. Egyváltozós valós-valós függvények differenciálszámítása
*Mindkét ismertetett lehetséges definíció megfogalmazása*

* Derivált megkeresesénék folyamatát nevezzük differenciálásnak

<!-- TODO valakitől el kell kérni órán 100 hogy megcsinálták -->

## 9.Egyváltozós valós-valós függvények monotonitása, konvexitása.

### 9.1 Egyváltozós valós-valós függvények monotonitása

#### 9.1.1 Monoton növekvés

Az $f(x)$ függvény monoton növekszik egy intervallumon, ha az intervallum minden pontján értelmezzük, és igaz, ha $x_1\leq x_2$, akkor $f(x_1)\leq f(x_2)$.

#### 9.1.2 Monoton csökkenés

Az $f(x)$ függvény monoton csökken egy intervallumon, ha az intervallum minden pontján értelmezzük, és igaz, ha $x_1\geq x_2$, akkor $f(x_1)\geq f(x_2)$.

#### 9.1.3 Szigorú csökkenés, növekedés

Egy függvény akkor növekszik/csökken szigorúan, ha nincsen két egyenlő függvényérték az adott intervallumon

### 9.2 Egyváltozós valós-valós függvények konvexitása

#### 9.2.1 Konvex

* Egy függvény **konvex** az $I\rightarrow R$ intervallumon, ha a függvénygörbe két végpontját összekötő szakasz a függvénygörbe **fölött** halad át.
* Magyarul, ha vizet öntünk a függvénybe megmaradna benne, avagy boldog a függvény: $\smile$
* Második derivált értéke: **nem negatív**
$$\forall x\in I\rightarrow R, f''(x)\geq 0\Leftrightarrow f(I\rightarrow R)\ konvex$$

#### 9.2.2 Konkáv

* Egy függvény **konkáv** az $I\rightarrow R$ intervallumon, ha a függvénygörbe két végpontját összekötő szakasz a függvénygörbe **alatt** halad át.
* Magyarul, ha vizet öntünk a függvénybe kifolyna belőle, avagy szomorú a függvény: $\frown$
* Második derivált értéke: **nem pozitív**
$$\forall x\in I\rightarrow R, f''(x)\leq 0\Leftrightarrow f(I\rightarrow R)\ konvkáv$$

## 10. Lokális szélsőérték és az inflexiós pont definíciója


### 10.1 Lokális szélsőérték definíciója

* Az a pont ahol az **első derivált** előjelet vált
* Ha negatívból, pozitívba megy át, akkor: **lokális minimum**
* Ha pozitívból, negatívba megy át, akkor: **lokális maximum**

### 10.2 Inflexiós pont definíciója

* Ebben a pontban vált a függvénygörbe görbületet (konvexitás, konkávitás)
* Az a pont ahol a **második derivált** előjelet vált

## 11. Riemann-szerinti integrálhatóság fogalma

* Integráljuk az $f(x)$ függvény az $[a;b]$ intervallumon
* $a$ az integrálás alsó, míg $b$ az integrálás felső határa
* Osszuk fel az $[a;b]$ intervallumot $n$ részre
$$ F_n=(x_0,x_1,x_2,\dots,x_n),\ |F_n|=n$$
$$a=x_0\lt x_1\lt x_2\lt \dots \lt x_n=b$$
* A felosztás finomságának nevezzük a felosztás legnagyobb részintervallumának hosszát. Jele: $d(F_n)$
* Mindegyik $[x_{i-1};x_i]$ részintervallumból kiválasztjuk az infimumát (legkisebb értékét) $\xi_i$ és a szuprémumát (legnagyobb érték) $\Xi_i$
* A részintervallumokra egyesével $f(\xi_i)$ és $f(\Xi_i)$ magas téglalapokat állítunk, majd összegezzük ezeket
$$s(F_n)=\sum_{i=1}^{n}f(\xi_i)(x_i-x_{i-1})$$
$$S(F_n)=\sum_{i=1}^{n}f(\Xi_i)(x_i-x_{i-1})$$
$$s(F_n)=\sum_{i=1}^{n}f(\xi_i)\Delta x_i$$
$$S(F_n)=\sum_{i=1}^{n}f(\Xi_i)\Delta x_i$$
* Az adott függvény akkor Riemann integrálható, ha:
$$\exists!\lgroup s(F_n)\leq I \leq S(F_n)\rgroup$$
* Ezt az $I$ számot hívjuk az $f(x)$ függvény Riemann integráljának
$$I = \int_a^b f(x)\ dx$$


## 12. Határozott integrál, primitív függvény

### 12.1 Primitív függvény

* A primitív függvény olyan $F(x)$ függvény amit, hogyha lederiválunk az eredeti $f(x)$ függvényt kapjuk

$$F'(x)=f(x)$$

### 12.2 Határozott Integrál

* az $[a,b]$ intervallumon integrálunk.
* ha nincs meghatározva az intervallum, akkor az határozatlan integrál
* Függvénygörbe és az $x$ tengely által határolt terület kiszámítására használható egy adott intervallumon
* Ha a függvény alatti terület az $x$ tengely alatt helyezkedik el értéke negatív lesz
* Newton-Leibniz formula:
$$ \int_a^bf(x)\ dx = [F(x)]_a^b=F(b)-F(a)$$

## 13. Improprous integrálok *"fő típusainak"* definíciója

* Végtelenbe elnyúló integrálok
* Ha $-\infin$-től $+\infin$-ig szeretnénk integrálni, kettéosztjuk a függvény, és összeadjuk a két integrál eredményét

$$\lim_{b\rightarrow\infin}\int_a^b f(x)\ dx$$
$$\int_a^{\infin} f(x)\ dx$$


## 14. Vektorok lineáris összefüggésének és függetlenségének definíciói

Lineáris algebrában a vektorok egy halmazát, linárisan függetlenek nevezzük, ha egyikük sem fejezhető ki a többi vektor lineáris kominációjaként. Ellenkező esetben lineárisan összefüggőnek.

## 15. A skaláris szorzat és tulajdonságai, kiszámítása

* Két $a, b$ vektor egymással $\theta$ szöget zár be, ekkor a skaláris szorzatuk:

$$a\cdot b = |a|*|b|*\cos(\theta)$$

* Vektorpárokat valós számokra képez

* Két dimenziós vektoroknál, ahol ismert mindkét vektor mindkét koordinátája:

$$a\cdot b = a_1*b_1+a_2+b_2$$

* n dimenziójú vektorokra:

$$a\cdot b = \sum^n_{i=1}a_i*b_i$$

* Ha a két vektor párhuzamos egymással:

$$\cos(0°)=1$$

$$a\cdot b= |a|*|b|\therefore a\cdot a=|a|^2$$

* Ha két vektor merőleges egymásra: ($\theta = 90°$)

$$a\cdot b = 0 \because \cos(90°)=0$$

# Tételek

## 1. Bernoulli-egyenlőtlenség és legalább egy alkalmazás

$$\forall h \geq -1 \land n\in\N$$
$$ (1+h)^n \geq 1+nh$$

### 1.1 Alkalmazása

* Egy hatványfüggvényt alulról tudunk vele becsülni

* Határértékszámításnál

* Exponenciális függvényeknél

## 2. Bolzano Weierstrass tételei

* **Végtelen korlátos sorozatból mindig kiválasztható konvergens részsorozat.**
<!-- TODO befejezni -->

## 3. Inverz függvények differenciálási szabályai



## 4. Az öszetett függvény differenciálási szabályai

## 5. Rolle-tétele, egy példa, az alkalmazása

## 6. Lagrange-féle középértéktétel

## 7. Cauchy-féle középértéktétel

## 8. Lokális szélsőérték létezésének elégséges feltétele

## 9. Lokális konvexitás elégséges feltétele

## 10. Az inflexiós pont létezésének elégséges feltétele

## 11. Bernoulli-L'Hopital szabály

## 12. Newton-Leibniz szabály

## 13. Helyettesítéses és paciális integrálás elve

## 14. Ívhossz, forgatástest térfogata, forgástest palást, szektorterület kiszámítási módja

*Paraméteres megadás esetén is*

