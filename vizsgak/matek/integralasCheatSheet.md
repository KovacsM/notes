# Integrálási szabályok, tudnivaló

## Alapintegrálok

$$\int x^n dx =\frac{x^{n+1}}{n+1} + c \quad n \ne -1 $$
$$\int \frac{1}{x} dx = \ln|x|+c$$
$$\int e^x dx = e^x +c $$
$$\int a^x dx = \frac{a^x}{\ln a}+x$$
$$\int \cos(x) dx = \sin(x)+c$$
$$\int \sin(x) dx = -\cos(x)+c$$
$$\int \frac{1}{\cos ^2x} dx = -\cot(x)+c$$
$$\int \frac{1}{1+x^2} dx = \arctan(x)+c$$

## Alapintegrálok Lineáris Helyettesítései

$$\int (ax+b)^n dx =\frac{(ax+b)^{n+1}}{n+1} * \frac{1}{a} + c \quad n \ne -1 $$
$$\int \frac{1}{(ax+b)} dx = \ln|ax+b|* \frac{1}{a} +c$$
$$\int e^{(ax+b)} dx = e^{(ax+b)} * \frac{1}{a} +c $$
$$\int a^{(ax+b)} dx = \frac{a^{(ax+b)}}{\ln a}+x$$
$$\int \cos(ax+b) dx = \sin(ax+b))* \frac{1}{a} +c$$
$$\int \sin(ax+b) dx = -\cos(ax+b)* \frac{1}{a} +c$$
$$\int \frac{1}{\cos ^2(ax+b)} dx = -\cot(ax+b)* \frac{1}{a} +c$$
$$\int \frac{1}{1+(ax+b)^2} dx = \arctan(ax+b)* \frac{1}{a} +c$$

## Integrálási szabályok

### Konstans szoró kivehető

$$\int c\cdotf = c \int f$$

### Tagonként is lehet integrálni

$$\int f + g = \int f + \int g$$ 

###  Szorzást ha lehet végezzük el!

## függvény hatványa szorozva a deriváltjával

$$\int f^a \cdot f' = \frac{f^{a+1}}{a+1} + c$$

## Parciális integrálás

$$\int f \cdot g' = f \cdot g - \int f' \cdot g

* Mikor célszerű használni

## Tört feldarabolása

$$\int \frac 
