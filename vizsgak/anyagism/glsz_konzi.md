# Gillemont László Szakkollégium Vizsga Konzultáció

## Lehűlési görbék

* Nagy feladat a végén
* Vas szén álapotábrákat kell tudni fejből értékekkel is
* Behúzzuk az ábrán azt a függőleges vonalat ami az x tengelyt a megfelelő helyen metszi (összetétel)
* Buza Gábor videói nagyon faszák
* F + Sz = K + 2
* SZ: Szabadsági fokok
* F: a változó Fázisok száma
* Szabadságfok
* Váltó pontokon több átmenet is lehet
* Kompnensek száma (K)
* |+ 2 helyett +1 mert a nyomást állandónak vesszük
* Lehűlési görbe: 0 konstans, 1 konkáv, 2 konvex (Utána kell nézni)
* Táblázatba vesszük
* Szövetelemek mennyisége a fázisok menniségére utal
* Perlit egy szövetelem de 2 fázis
* Folyadék nem szövetelem

---

1. Vas szén állapotábra
2. Egyenesen pontokat felvesszük
3. Táblázatot készítünk
4. K mindig 2
5. F: Fázisok száma (Hány féle cucc van, átmeneti részeken 2, 3)
6. Fázisok nevét is meg lehet nevezni
7. F + SZ = K + 1
8. Átrendezzük: SZ=K+1-F
9. K mindig 2 így: SZ=3-F
10. Táblázatban
* Perlit és Ledeburi: 2 alkotója van!!!
---
* Minél gyorsabb a lehűlé stöbb, kisebb kristály, gyorsnál nagyobb kevesebb kristály

---

* Eutektikum Eutoktonid, senem szilárd oldat senem mechanikus vesgyület, kerülvevő kristályok vegyülete
* Szoba hőmérsékleten: A szén képes a ferrit szerkezeteket ausztenit szerkezetekké alakítani
* Ötvözők növelik a fajlagos ellenállást, hővezető képességet $\Rightarrow$ Kristályhibát képez

## Megeresztés

* Martenzit kicsit megolvad, ridegség csökkentése, szívósság növelése

## Nemesítés

* Edzés majd megeresztés

## Hiszterézis Görvbe

* egyszer felmágnesezzük, hogyan tudjuk eltüntetni

## Mágneses tulajdonságok

* Fero Para Dia
* Domainek: Azonos irányű mágneses spin (elektron spin)
* Barkhausen Zaj


