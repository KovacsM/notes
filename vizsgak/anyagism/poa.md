# Anyagismeret Vizsga Plan of Attack

## Előadás anyagok

* [ ] 1. Bevezetes
* [ ] 2. Ideális rács
* [ ] 3. Kristályhibák
* [ ] 4. Ötvözetek állapotábrája
* [ ] 5. FeC állapotábrák
* [ ] 6. Fázisátalakulások
* [ ] 7. Acélok nemegyensúlyi átalakulása
* [ ] 8. Szilardsagnoveles
* [ ] 9. Ötvözetek
* [ ] 10. Mechanikai tulajdonságok és vizsgálatuk
* [ ] 11. Technológiák
* [ ] 12. Leromlás
* [ ] 13. Vezetés
* [ ] 14. Mágnesesség

## Labor Silabuszok

* [ ] 1. Szakítóvizsgálat
* [ ] 2. Mikroszkóia
* [ ] 3. Állapottényezők és keménységmérés
* [ ] 4. Szilárdságnövelés és újrakristályosodás
* [ ] 5. Hideg-Meleg alakítás
* [ ] 6. Hegesztés

## Egyéb segédletek

* [ ] Prohászka fejezetek
* [ ] Feszültségkoncentrációs ábra
