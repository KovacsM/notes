# Prohászka: Bevezetés az Anyagtudományba I. fejezetek végéből a definíciók

## I. Bevezetés

Kovalens kötés
: Legerősebb típusú kötls, melyben két vagy több atom vesz részt és ezek között a kötőelektronok meghatározott és kötött pályán mozognak

Ionos Kötés
: Az ionok között megvalósult kötéstípus, melyben elektroszatikus kötőhatás tartja össze a kristályt. Az ionokat az hozza létre, hogy az elektronpozitív atomok, egy vagy két elektront leadnak, és ezeket az elektronnegatív atomok meggötik.

Fémes Kötés
: Ebben a kötéstípusan a kötőelektronok, melyek az egész anyaghoz tartoznak és csaknem szabadon mozognak az anyag belsejébe, közel homogén negatív töltésű teret alkotnak, ami összetartja a pozitív ionokat.

Van der Waals-kötés
: Másodlaos, gyenge kötés. Időleges vagy állandó elektromos dipólusok vonzása következtében jön létre.

Kötési Szög
: Egy atomot két legközelebbi szomszédjával összekötő egyenesek közötti szög.

## II. Kristálytani alapismeretek

Térrács
: Kiterjedés nélküli pontok szabályos rendje a térben, amit három transzlációs vektor határoz meg.

Elemi cella
: A térrács vagy kristály legkisebb egysége, amit három transzlációs vektor definiál, és magán viseli a rács minden jellegét.

Primitív cella
: Olyan elemi cella, mely csak a sarkain tartalmaz rácspontot.

Miller-index
: A rács síkjait és iránait meghatározó számhármas.

Koordinációs szám
: Egy atom legközelebbi szomszédjainak száma.

Atomátmérő
: Két legközelebbi atom távolsága a kristályban.

Rácsállandó, *rácsparaméter*
: Az elemi cella élhosszúsága.

Egyszerű köbös rács
: Az elemi cellája primitív cella.

Térben Középpontos Rács
: Az elemi cella térközepén is tartalmaz rácspontot.

Felületen Középpontos Rács
: Az elemi cella felületeinek közepén is van rácspont.

Hexagonális Rács
: Az elemi cellája egy 120°-os szöget bezáró rombuszalapú egyenes hasáb.

Legszorosabb illeszkedésű hexagonális rács
: Az a sík a kristályban, melyben minden atomnak a 6 legközelebbi, egymást érintő szomszédja van.

Legsűrűbb illeszkedésű irány:
: Az az irány a kristályban, melyben a merev gömbnek a tekintett atomok érintik egymást.

## III. Kristályhibák


