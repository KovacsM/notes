| Tantárgy neve | link |
|---------------|------|
| Gépszerkesztés alapjai | [gt3.bme.hu](https://gt3.bme.hu/letoltesek.php?lepes=1&tscroll=200&lscroll=0&list_tscroll=900&list_lscroll=0&lapozo=%5B4%2C33%2C2%5D) |
| Anyagismeret | [att.bme.hu](https://www.att.bme.hu/oktatas/bmegemtbma1/) |
| Mérnöki fizika | Teamsen fileok között |
| Matematia Gyakorlat | [math.bme.hu](https://math.bme.hu/~geom/matdoc/HA/) |
| Statika | [edu.gpk.bme.hu](https://edu.gpk.bme.hu/course/view.php?id=190#section-3) |

