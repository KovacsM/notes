# Cheat sheet a markdown matekos formulákhoz

Ebben a dokumentumban a markdownban használható matematikai jelölések vannak összeszedve egy helyre.
A jelölések inline formátumban vannak, van egy példablock féle megjelenítésre is.

### Formátumok

`$$`: **Block** centered, sort-tör<br>
`$`: **inline** sorban is elhelyezhető, balra rendezett

### Alap operációk

|Jelölés|Eredmény|
|-|-|
|`\sqrt(a)`|$\sqrt(a)$|
|`x^y`|$x^y$|
|`\frac{a}{b}`|$\frac{a}{b}$
|`\sqrt[a]{b}`|$\sqrt[a]{b}$
|`\log_{n}(a)`|$\log_{n}(a)$
|`\ln a`|$\ln(a)$|

### Szögfüggvények

|Jelölés|Eredmény|
|-|-|
|`\sin(\theta)`|$\sin(\theta)$|
|`\cos(\theta)`|$\cos(\theta)$|
|`\tan(\theta)`|$\tan(\theta)$|
|`\sin^{-1}(\theta)`|$\sin^{-1}(\theta)$|
|`\cos^{-1}(\theta)`|$\cos^{-1}(\theta)$|
|`\tan^{-1}(\theta)`|$\tan^{-1}(\theta)$|

##### Példa

$$
    \cos^{-1}(\frac{\pi}{2}-\theta) \geq \pi
$$

### Mátrixok

TODO: Kelleni fog még egy snippet hozzá, hogy a begin és end tageket ne kelljen leírni.

**Csakis block styleban értelmezi a gitlab a mátrixokat! Inlineban nem lehet leírni!!**

`&`: új cella<br>
`\\`: új sor

```
    \begin{bmatrix}
        a&b&c&d\\
        d&a&b&c\\
        c&d&a&b\\
        b&c&d&a
    \end{bmatrix}
\Longleftrightarrow
    \begin{bmatrix}
        a&b&c&d\\
        d&a&b&c\\
        c&d&a&b\\
        b&c&d&a
    \end{bmatrix}
```

$$
    \begin{bmatrix}
        a&b&c&d\\
        d&a&b&c\\
        c&d&a&b\\
        b&c&d&a
    \end{bmatrix}
\Longleftrightarrow
    \begin{bmatrix}
        a&b&c&d\\
        d&a&b&c\\
        c&d&a&b\\
        b&c&d&a
    \end{bmatrix}
$$

### Relációs jelek

|Jelölés|Eredmény|
|-|-|
|`=`|$=$|
|`\neq`|$\neq$|
|`\gt`|$\gt$|
|`\lt`|$\lt$|
|`\geq`|$\geq$|
|`\leq`|$\leq$|

if $a \lt b$ then $b \neq a$

### Halmazok jelei

|Jelölés|Eredmény|
|-|-|
|`\subset`|$\subset$|
|`\not\subset`|$\not\subset$|
|`\supset`|$\supset$|
|`\subseteq`|$\subseteq$|
|`\not\subseteq`|$\not\subseteq$|

### Logika jelek

|Jelölés|Eredmény|
|-|-|

### Források

[oeis.org](https://oeis.org/wiki/List_of_LaTeX_mathematical_symbols)<br>
[file](./README.md)
