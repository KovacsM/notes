# Első Mechatronika ZH fogalmak, tudni való dolgok

## Fogalmak

### Jel

Változó Fizikai mennyiség absztrakt információ tartalma

### Bemenetek

Valós fizikai rendszerre ható és időben változni képes kényszerek

### Kimenetek

Kimenet
: Valós fizikai rendszereknek a fiziai kényszerek hatására bármely báltozás

Fizikai kimenet
: olyan kimenet amit az adott vizsgálatban közvetlen vagy közvetettem mérünk

### Rendszer

Egy fizikai rendszer valamilyen pontosságú modellje, bemenő és kimenő jelek közti matematikai kapcsolat

Valós Fizikai Rendszer
: Mérhető külső hatásra mérhető módon változi

Dimenziója
: Szükséges minimális állapotválzotók száma

### Paraméterek és változók

Valós fizikai rendszert leíró egyenletek eggyütthatói

### Vezérlés <-> Szabályozás

Vezérlés
: Változó zavarokat nem képes kezelni (előre kell minden zavart számjtásba venni), mivel ilyenkor nincs visszacsatolás a kimenetből (nyílt hajtáslánc), csak **determinisztikus** zavarjelek kezelhetőek.

Szabályozás
: Változó zavarokat is képes kezelni mivel  visszacsatolunk a kimenetből (zárt hajtáslánc)

* pl:
    * Vezérlés ha bekapcsoljuk a radiátort, de az nem tudja hány fok van a szobában, ha sokan vagyunk benne vagy éppen megy a számítógép melegebb lesz mint alap állapotban. Ha pontos hőmérsékletet akarunk elérni figyelembe kell venni mindent.
    * Szabályozás, ha mérjük a szoba hőmérsékletét, így bármi is történik a szobában kb. ugyan az a hőmérséklet lesz mindíg. Zavarokat tudjuk kezelni.
    * Szabályozás: Emberi pupilla tágulása, szűkülése

* Vezérlést egyszerűbb rendszerekre lehet használni ahol kevés a zavar.
* Szabályozást akár sokkal komplexebbekre is.

### Mechatronika

Intelligens gépek tudománya amely a gépészet, elektrotechinka és a számítógépes irányítás egymás hatását erősítő integrációja a termékben.

### Pozíciónáló asztal

3. előadás diák: 12-15. oldal (elv csak akkor kell tudni ha ki akarod maxolni)

### Paraméterek változók

A valós fizikai rendszert leíró **egyenletek eggyütthatóit** paraméternek nevezzük

#### Elosztott

A tér minden pontjában meghatározunk valamilyen matematikai összeüggést

#### Koncentrált

A vizsgált valós fizikai rendszer összefüggéset azok jellegétől függően egy adott térrészben **összegezzük vagy kiátlagoljuk** és **egyetlen egyenlettel helyettesítjük**.

### Valós Fizikai rendzer

* Fizikai objektum
* **Mérhetű** külső kényszerek hatására **mérhetően** változik

### Szuperpozíció elve

* A bemeneti jel arányos a kimenettel, valamint a bemeneti jelek összegezhetőek

* Diákból (4.ea: 9.d):
    * Egy matematikai modell lineáris jellegét, vagyis a szuperpozíció elvének érvényességét matematikai eszközökkel tudjuk igazolni.
    * Egy valós rendszerben csak egy adott működési tarományban bizonyos pontosággal lehet, és annak lineáris jellegét megfelelően megtervezett mérési sorozattal tudjuk ellenőrnizni.

* **Linearitásnak** a fogalma, hogy érvényes rá a szuperpozívió elve

### Determinisztikus, Sztohasztikus rendszer

#### Determinisztikus

A rendszer teljes ismeretében egy adott bemenetre mindíg egy adott értéket kapunk

#### Sztohasztikus

Egy konkrét bemenőjelre adott válaszát nem lehet előre meg mondani, csak annak valószínűség eloszlását. Tartalmaz valamilyen véletlenen alapuló paramétert.

## Állapotfüggvények elemzési szempontjai

### 1. Linearitás

(Lineáris, nem Lineáris)

* Ha Lineári **szuperpozíció** elve érvényes rájuk ( egyben definícióként is használható )
* Nem lineáris ha:
    * Van az egyenletben $sin(y(t))$
    * Ha időtől függő értékkel ($k,t$) szorzom meg a függvényt ($y()$ -on kívül)
    * Ha konstans értéket adok hozzá a függvényhez pl.: $y(t)=3+a*u(t)$

### 2. Statikus, Dinamikus

(Statikaus, Dinamikus)

Statikus
: Bármely pillanatban a bemenő jelek pillanatnyi értkéei egyérteműen meghazátozzák az adott pillanatban kimenő jelek értékeit.
: Nincsen memória, Előző állapotok nincsenek hatással a következő állapotra.

Dinamikus
: Valós Fizikia rendszerek időbeli lefolyását is reírja, jellemzően időszerinti differenciál egyenletekkel
: Memória jellege van!

### 3. Idővariencia
( Időinvariáns; Idővariáns, vagy Nem időinvariáns )

Időinvariáns
: Ha egy $U(t)$ jelre válasza $y(t)$ és egy $U(t-T)$ bemenő jelere a válasza $y(t-T)$ és pontszerű időbeli eltolással megkapható.
: Nem függ az időtől a kimenő jel értéke.

### 4. Autonómitás
(Autonóm, nem Autonóm)

* Egy rendszer autonóm, ha Időinvariáns és az $u(t)$ egy konstant gerjesztés
* Erős tipp, ha Dinamikus a rendszer akkor autonóm is, ha Statikus akkor nem autonóm. (Nem láttam még olyan példát amikor nem lett volna így)

## Források

* Senior Kozni
* Gépész drive: alapkérdések-kidolgozás-barna.pdf
* Órai jegyzetek
* Előadás diák (teamsen)
