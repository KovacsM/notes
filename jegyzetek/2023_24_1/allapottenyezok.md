# Állapotényezők

1. Hőmérséklet
2. Alakváltozási sebesség
3. Feszültség állapot

* Anyag vagy ridegen vagy szívósan viselkedik
* Állapottényezőktől függ

* Rideg viselkesdés nem jó

## Hőmérséklet

* Charpy vizsgálat (Charpy-féle ütővizsgálat)
* Csak adott méretű próbatesteket lehet összehasonlítva
* Ütőmunka ($KV$) egyre kisebb minél alacsonyabba hőmérséklet
* Meg kell találni a pontot amin már átmegy a másik állapotba (rideg-képlékeny átmenet) ($TTKV$)
* Felületen Középpontos Köbös anyagokat nem érinti a hőmérséklet általi ridegedés

## Alakváltozás sebesség

* Fajlagos törésmunka a lényeg
* Lehet hogy szilárdsága nagyobb rideg viselkedéskor
* de a fajlagos törési munka kisebb lesz

## Feszültségállapot

* Ha teljes lemez mindenhol ugyan anyi a feszültség
* ha van benne egy kör akkor feszültség pontok jönnek létre
