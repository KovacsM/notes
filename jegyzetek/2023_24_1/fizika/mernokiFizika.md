# Fizika
**Axióma rendszerekből áll**
* Mechanika (Newtoni axióma rendszer)
* Elektrodinamika
* Termodinamika (Fő tételek)
* Relativitáslemélet
* Kvantumelméletek

## Mértékegységek rendszere
**SI Mértékegységek Rendszere**

| Mennyiség | Jele | Mértékegysége |
|-|-|-|
|Hosszaság| $s$ | $[m]$ (méter) |
| idő | $t$ | $[s]$ (secundum) |
| tömeg | $m$ | $[kg]$ (kilógramm) |
| Síkszög | $\alpha$ | $[\pi]$ (radián) |
| Térszög | $\alpha$ | $[sr]$ |

## Koordinátarendszerek
**Kétdimenzis Descarte-féle koordinátarendszer**

* 2D (Kétdimenzis Descarte-féle koordinátarendszer)
* 3D

## Kinematika
* Út: $\Delta s$
* Elmozdulás: $\Delta \vec{r}$ (elmozdulásvektor
* Átlag sebesség: $v=\frac{\Delta s}{\Delta t}$ $[\frac{m}{s}]$
* Pillanatnyi sebesség: $v=\frac{\Delta s}{\Delta t}$ de $\Delta t$ egyre kisebb
* $\vec{v} = \frac{\Delta \vec{s}}{\Delta t}$
* Gyorsulás $\vec{a} = \frac{\Delta \vec{v}}{\Delta t}$

### Egyenes Vonalú Egyenletes Egy Dimenziós Mozgás

* $\vec a = 0$
* Egydimenziós koordinátarendszert veszünk fel
* Kezdő helye: $x\scriptscriptstyle 1$
* Sebesség idő grafikon
* Függvény alatti terület a megtett út $(\vec s)$

### Egyenes Vonalú Egyenletes Változó Mozgás

* $\Delta \vec a = 0$, de $| \vec a|  \not = 0$
* Sebesség, idő grafikon alatti terület az elmozdulás

### Egyenletes Körmozgás

* $r$ sugarú kör
* Kötponti szög radioán $(\pi)$
* **SZABADVEKTOROKAT ÁTNÉZNI**
* Centripetális gyorsulás


