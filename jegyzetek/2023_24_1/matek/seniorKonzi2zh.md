# ZH második Senior Konzi

### Infrakciós pont
* Második derivált
* Hol lesz 0?
* $y-y_0=m(x+x_0)$ nemtommi?

### Körből körcikk mekkora lesz a legnagyobb alfa PROBAZH

* egység sugarú Körből körcikket vágunk ki mekkora lesz a legnyagyobb területű
* Alfa függvényében térfogatát kiszámolni egy körrcikknek

$$V=\frac{r^2\pi*m}{3}$$
$$R^2=m^2+r^2$$
$$A=\pi*r*R$$
$$V_\alpha=R^2*\pi*\frac{\alpha}{360°}$$
$$r = \frac{\alpha}{360°} \Rightarrow m = \sqrt{R^2-r^2} = \sqrt{1-\frac{\alpha}{360°}}$$
$$V_\alpha=\frac{(\frac{\alpha}{360°})^2*\pi*\sqrt{1-(\frac{\alpha}{360°})^2}}{3}$$
* Alakítsuk majd deriváljuk le
* Majd keressük meg hol egyenlő nullával
$$\pm293,939°$$

### Teljes függvény vizsgálat

$$f(x)=x-arctg(x)$$

#### Éretelmezési tartomány
$$x\in\R$$
#### Értékészlet
$$y\in\R$$
#### Zérúshelyek
* Hol lesz nulla a föggvény
* nem gázos
#### Paritás
* Páros-e vagy nem
$$f(-x) = f(x) \Rightarrow Páros$$
$$f(-x)=-f(x) \Rightarrow Páratlan$$
#### Periodikusság
* Be kell elyttesíteni periódusonkét hogy bizonyítsuk
* ZHban be kell bizonyítani, de az ábrából is lehet megmondani
#### Határérték
$$\lim_{x\rightarrow\infin}{x-arctg(x)}$$
##### L'Hopital:
* Külön külon deriváljuk a számlálót és a nevezőt
* Elvégezzük az osztást, majd a kapott érték határértékét keressük $\pm\infin$-ben is
#### Szélsőérték
#### Monotonitás, Infelxiós pont, Konvexitás
* Deriváljuk le
* Éredemes alakítgatni
* Majd mégegyszer deriváljuk
* Mikor lesz 0 a második derivált
* Táblázatba érdemes beírni, hogy negatív pozitív vagy 0 az első és a második derivált

|Második derivált értéke|konvexitás|
|-------|-------|
|Negatív| konkáv|
|Pozitív| konvex|
|Nulla| Infelxiós pont|

* Konvexitást válthat még akkor is ha nincs értelmezben abban a pontban

#### Aszimptoták
* Olyan egyenes amihez tart a függvény de sohasem éri el
$$y=k*x+m$$
$$\lim_{x\rightarrow\infin}\frac{f(x)}{x}$$
