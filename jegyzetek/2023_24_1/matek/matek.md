# Halmazok

### Alapfogalmak

##### 1. Halmaz:
    halmazról beszélünk, ha bármely dologról el tudjuk dönteni, hogy hozzátartozik avagy sem
##### 2. hozzátartozás:
    x hozzátartozik az A halmazhoz,ha A-nak eleme
* jel: A,B... $$x\inA , x\ninA

##### Meghatározottsági axióma

Az A és B halmazok egyenlőek, ha A minden eleme B-hez is hozzátartozik, és B minden eleme A-hoz is hozzátartozik. A=B

##### Halmazműveletek

###### 1. Unió
* Az A és B halmazok uniója azon elemek halmaza, amelyek legalább az egyik halmazhoz tartozik
* $$A\unioB={x|x\in a \or x\inB$$

###### 2. Metszet

* A és B halmazok szorzata (metszete) azon elemek  összessége amelyek A-hoz és B-hez is hozzátartoznak
* $$A\metszetB = {x|x\inA \and x\inB}$$

###### 3. Különbség

* Az A ls B halmazok különbsége azon elemek összessége amely A-hoz hozzátartozik, de B-hez nem
* $$A\különbB = {x\inA \and x\ninB}$$

###### 4. Komplementerhalmaz

* Komplementerhalmaza B-nek az A-ra vonatkozó komplementer halmaza $$A\különbB$$

###### 5. Részhalmaz

* $$\every$$ Minden egyes, univerzális kvantor
* Ha B halmaz mindenegyes eleme A-hoz is hozzátartozik, akkor B részhalmaza A-nak
* 

###### 6. Üres halmaz

* Ha egy halmaznak nincs eleme, akkor üres halmaznak nevezzük
* Jele $$\empty$$

###### 7. Diszjunkt halmazok

* Ha A és B halmazoknak nincs közös eleme, akkor diszjunktnak nevezik őket
* $$A\unioB=\empty$$

###### Megj: De Morgan azonosságok

* A halmaz komplementerének a komplementere az A halmaz
* A komplementerének és B komplementerének Uniója az A unió B
* A komplementerének és B komplementerének Metszete az A metszet B
* **Általánosítható tetszőleges indexhalmazokra is**

##### Halmazok Direkt Szorzata

* **Def.:** Tetszőleges két elemből x,y-ból képzett (x,y) párt, **rendezett párnak** nevezzük; a sorrendjük meghatározott.
* **Def.:** Legyenek A és B halmazok, mindazon rendezett párok halmazát, amlyeknek az első eleme A-hoz tartozik és a második B-hez A és B halmazok **direkt szorzatának** nevezzük.
* $$ A x B = { (x,y) | x\inA, y\inB}
* **Def.:** Az A halmaz értelmezett kétváltozós **relációnak nevezzük az $AxA$ részhalmazt
* **PL.:** Eggyenlőség, vagy a teljes $AxA$
* **Def.:** Egy A halmaz beli relácit ekvivalenciának nevezünk, ha teljesülnek az alábbiak:
    1. $x ~ x$ reflexív
    2. $x ~ y, y~\Z \rightarrow x ~ z$ tranzitív
    3. $x~y \rightarrow y~x szimmetrikus
* **Pl.:** Legyen $A=Z$ egész számok, reláció legyen az öttel való osztahtóság $\rightarrow x~y$, ha 5-tel osztva ugyan annyi a maradéka Ekvivalencia.
* Részhalmazokat csinál a $\Z$ halmazban, maradékok alapján
* Minden Ekvivalenciális egyenlőség részhalmazokra osztja fel az adott halmazt
* **Def.:** 
    1. $\forall$ X-beli Ekvivalencia relációhoz hozzátartozik X-nek Diszjunkt halmaokra való lefedő felosztása.
    2. X-nek $\forall$ Diszjunkt halmazokra való lefedő felosztása, indukál egy Ekvivalencia relációt, ami őt előállítja.

##### Függvény

* **Def.:** Legyenek $A$ és $B$ tetszőleges halmazok. Ha $A$ halmaz, $\forall$ eleméhez hozzárendelünk pontosan egy $B$-beli elemet, akkor ezt a hozzárendelést, **függvénynek** nevezzük
* Az $A$ halmaz a függvény értelmezési tartománya, Jele: $Df$
* A felvett $B$-beli elemek összessége a értékkészlet $Rf\subseteqB$
