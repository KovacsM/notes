# Mechatronika Alapjai
```
Dr. Huba Antal
```
### Jól jöhet ha megtanuljuk
* Lajlice transzformációk
* Fourier transzformációk

### Célja
Különféle fizikai/technikai rendszerek **vizsgálata és modellezése**, annak érdekében, hogy a rendszer tulajdonságainak ismeretében lehetőségünk nyúljon **beavatkozni,** a rendszer viselkedésébe, úgy hogy egy általunk definiált **cél megvalósuljon**
* Vizsgálattal és modellezéssel foglalkozunk mecha alapokon, és mechatronikán
* Beavatkozás és cél megvalósítása: Rendszer- és inrányítástechnika tanrtárgy alatt foglalkozunk

### Mi is az a rendszer?
Olyan fizikai objektum amely mérhető belső kényser hatására mérhető módon megváltozik

* Rendszernek bemenetei vannak (akár több is)
* ezekbe a bemenetekbe beavatkozhatunk aktuátorokkal (elektronika, elektrotechnika)
* Bemenet hatására kimeneteket érzékelhetünk
* Ezeket mérjük (szenzorokkal)
* Beavatkozás és mérés közötti kapcsolat: Általában programozott processzoron keresztül történik (informatika)

|Eszköz neve| eszköz célja | kimenet | Bemenetek (mivel lehet beavatkozni) | Mérések | Programozott kapcsolat |
|-|-|-|-|-|-|
| Segway | Függőleges pozíció megtartása | függőlegessel való bezárt szög | kereket hajtó villanymotor | kerék fordulatszáma, szögelfordulás | Ha nagy a bezárt szög $\rightarrow$ legyen nagy a motor szögsebessége |
| SpaceX Rakéta landolás | megfelelő lassítással, függőleges pozícióban landolás | szög függőleges sebesség | megfelelő irányba anyag kilökés | szög elfordulás & sebesség | ha nő a szög $\rightarrow$ anyag kilökése megfelelő irányba |
| Mercedes aktív felfüggesztés | Függőleges mozgás minimalizálása | Függőleges pozíció | Felfüggesztés merevségének állítása | Autó előtti út viszonyok (optikai) | úthibák függvényében $\rightarrow$ felfüggesztés merevségének állítása |
| Drón | Adott ember követése, adott távolsággal | mért távolság | motorok | távolság | ha nő a távolság $\rightarrow$ motorokkal kompenzálni |
| Robotkar | Szerszámfely pontos pozíciója | 3D-s pozíció | csuklókat mozgató motorok | a csuklók szögelfordulása | adott pozíciók eléréséhez adott szöghelyzetbe kell a csuklóknak mozogni |

## Mi is az az irányítás

Egy folyamatba való beavatkozás, valamely cél elérése érdekében
* Pl.: 
    * Mosógép időprogram szerinti irányítása ( Mosás $\rightarrow$ Öblítés $\rightarrow$ centrifugálás )
    * Szoba fűtése hőmérséklet mérése nélkül
    * Szoba adott fokon tartása

* Két típusa létezik az irányításanak:
    * vezérlés
    * Szabályozás
        * Értéktartó
        * Követő (pl.: pupilla, telefon fényereje)


#### Vezérlés

**Vezérlő** -- _bemenet_ $\rightarrow$ **Folyamat** -- _kimenet_ $\rightarrow$
* Nyílt hatáslánc (nincs visszacsatolás)
* folyamat eredménye nincs hatással a folyamatra $\rightarrow$ irányított fizikai mennyiséget nem mérjük
* Új / nem modelezett zavarokat nem képes kezelni a rendszer (minden zavarra előre gondolni kell előre)
* Műszaki komplexitás nem lehet nagy (nem lehet sok zavar)
* Csak akkor tükéletesen pontos ha $\forall$ zavar jelet számításba veszünk
* Instabil rendszereket így nem lehet irányítani
* Bemenő jelekre azonnal reagál

#### Szabályozás

**Vezérlő** -- _bemenet_ $\rightarrow$ **Folyamat** -- _kimenet_ $\rightarrow$ (**Vissza a vezérlőbe és kimenet**)
* Kimenet vissza van csatolva a vezértlőbe
* Zárt hatáslánc
* kimenetet/eredményt mérjük
* zavarokat képes kezelni, ha a kimenetre hatással van
* Műszaki komplexitása alacsony (különbségképzés)
* Nem lehet vele tökéletes eredményt elérni, csak közelítünk
* Instabil rendszerek esetén alkalmazható
* Beavatkozáskor energiát közlünk a rendszerrel

#### Szabályozás felépítése

**A** ábra

#### Szabályozási kör hatásvázlata

**B** ábra

* Állandóan fut (rövid időközönként)

#### Villamosmotor fordulatszámának szabályozása

**C** ábra

#### Átviteli függvény

**Egy rendszer vagy folyamat bemenete és kimenete közti kapcsolatot írja le**

**D** ábra
* Jele: $W, G$
* $W = \frac{kimenet}{bemenet} = \frac{y}{u}$
* matematika -- fizikai átalakítást végző egység $\rightarrow$ nálunk szorzótag
* fizikai mennyiség absztrakt információtartalma / fizikai mennyiség értékváltozása, ami információt hordoz
* Jele: $x$

**E** ábra

* Wsz a szabályozott szakasz átviteli függvénye

* $Wsz = 500\frac{1}{min}/V$
* Wv érzékelő, mérőátalakító átvitel függvénye
* $Wv = 1\frac{mV}{1/min}$
* Wr szabályozó átviteli függvénye (V $\rightarrow$ V-be megy, nem kell mértékegység)
* $Wr = 20$ (arányos tag, megszorozza)

$$Xs(Xa,n)=?$$
$$Xs=Wsz * Xm$$
$$Xe=Wv * Xs$$
$$Xa,v=Wv*Xa,n$$
$$Xr=Xa,b - Xe$$
$$Xm=XR*WR$$

* Célunk hogy  akimenete meghatározzuk a bemenet függvényében

$$Xs=Wsz*WR*XR=Wsz*WR*(Xa,v-Xe)$$
$$Xs=Wsz*WR*(WV*Xa,n-Wv*Xs)$$
$$Xs=Wsz*WR*Wv(Xa,n-Xs)$$
$$Xs(1+Wsz*Wr*Wv)=Wsz*WR*WV*Xa,n$$
$$Xs=\frac{Wsz*WR*WV}{1+Wsz*WR*Wr}*Xa,n$$
$$Wcl=\frac{y}{u}=\frac{Wsz*WR*WV}{1+Wsz*WR*Wr}$$
* Rendszer átviteli függvénye $\rightarrow$ "Eredő függvény"

----

felnyitott kör: $Wr*Wsz*Wr=Wo$

$$Wsz = 500\frac{1}{min}/V$$
$$Wv=1\frac{mV}{1/min}$$
$$WR=20$$
$$Xs=\frac{Wsz*WR*Wv}{1+Wsz*WR*WV}*Xa,n$$
$$Xa,n=2000\frac{1}{min}$$
$$ Xs= \frac{500\frac{1}{min}/V * 1\frac{mV}{1/min}*20}{1+500\frac{1}{min}/V* 1\frac{mV}{1/min}*20}*2000=\frac{10}{11}*2000=1818\frac{1}{min}$$

* Az érték nem az amit mi szeretnénk

* $Xa,n = 2000$

|WR|Wce|Xs (1/min)|$Xs-Xa,n$ (1/min)|
|-|-|-|-|
|20|$\frac{10}{11}$|$1818$|$\frac{1}{11}Xa,n$|
|10|$\frac{5}{6}$|$1667$|$\frac{1}{6}Xa,n$|
|100|$\frac{50}{51}$|$1961$|$\frac{1}{51}Xa,n$|
|1000|$\frac{500}{501}$|$1996$|$\frac{1}{501}Xa,n$|

* Nagy szabályozó esetén heves lesz a viselkedés

#### Dinamikus rendszer időfüggvényei

* A rendszerünk értéke csak megközelíti a kívánt értéket kis hibahatár lesz
* És az elején túllövi/alálövi a kívánt értéket, de utána beáll a jó helyre (hibahatáron belülre)
* Állandósult állapoti érték a végén
* hibahatár által leírt sáv $\rightarrow$ referencia (kb. 5%)
* 0,05 Xa,n elérésének ideje a lappangási idő ($Tl$)
* Alsó hibahatár érintése $\rightarrow$ felfutási idő ($T
* Amikor a referencia sávokon belül marad $\rightarrow$ beállási idő, szabályozási idő ($Tsz$)
* Előtte tranziens idő, átmeneti, ideiglenes
* $Xmax$ a legnagyobb túllendüles
* Átmeneti függvény: egyik és másik állapot közti átmenet **egységugrás** gerjesztésére adott válasza
