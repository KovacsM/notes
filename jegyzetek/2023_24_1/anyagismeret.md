# Anyagismeret

## Anyagvizsgálat

* Roncsolásos (mechanikai vizsgálatok általában ilyen)
* Roncsolásmentes

## Fémek és ötvözetek

* Fémes kötés tartja üket össze
* Fém ha kémiai elem (periódusos rendzser)
* Ötvözetek ha fémes kötésű többkomponens, alap kötésű fémes kötésű legyen

## Kémiai Kötések Ideális Kristályrács

* Szilárd anyagokkal foglalkozunk (kondenzált anyagok)

* Olvadáspont egyenesen arányos a kötési energiával
* A rugalmassági modulusz is egyenesen arányos a kötési energiával

## Kristályszerkezetek

* Kristálycsíra $\rightarrow$ nucleus

## Bravais-rácsok

* térbeli alakzatok (általában hasábok)
* Alapi eltérések (rombusz alap, négyzet alap,...)

## Amorf ötövzetek alakváltozó képessége

* gyorshűtés $\rightarrow$ amorf marad a kristályrács, nem lesz szabályos
* Finemet (márkanév) ma is készül, lágymágneses vasmagoknak
* nincs képlékeny alakváltozás amorf testekben mivel diszlokáció nem lehet mivel nincs kristályszerkezet
* Ikerhatár csak felületen középpontosan körös

# Ötvözetek szerkezete

## Ötvözetek

* Több komponensből állnak
* Látszatra homogén, mikroszkóp alatt nem az!
* Ha véletlen kerül bele komponens az szennyezés
* Ha direkt akkor az ötvözetet javítja

### Ötvözetek előállítása

* Sokféle mód, anyag adottságai miatt van rá szükség

### Fázis
