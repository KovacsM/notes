# Bevezetés, Alapfogalmak

* Klasszikus Mechanikával foglalkozunk

## Mechanika

1. Folyadékok mechanikája ( nem foglalkozunk vele )
2. Szilárdtestek mechanikája:
    * Deformálhato testek:
        * Rugalmasságtan
        * Képlékenységtan
        * stb...
    * Merev testek:
        * Statikus ( nem mozdul meg )
        * Dinamika ( meg is mozdul ):
            * Kinematika ( hogyan mozog a test )
            * Kinetika ( Miért mozog, erők hatásai )

**Statikában merev testekkel nyugalmi állapotban foglalkozunk**

## Statika

**Merev testek egyensúlyhelyzetének avagy nyugalmihelyzetének vizsgálata**

### Merevtest

* Külső teher hatására nem változtatja meg az alakját
* vagy: Bármely két pontja között a távolság állandó marad

### Modell

A fizikai valóság egy adott szempontrendszer szerinti egyszerűsítése

* **Geometria modellezése**:
    * pl: földet pontszerű testként modellezzük a földet -> egyszerűsítünk
    * Állhatnak:
        * pont
        * vonal
        * hély
        * 3D-s test

* **Anyagimodell**:
    * Merev anyagmodell (ezt használjuk itt)
    * Rugalmas anyagmodell
    * Képlékeny

* **Terhelések**:
    * Pontszerű (lufi)
    * Él mentén megosztó (kés stb)
    * Felület mentén megoszló (tenyér asztalon)
    * Térfogat mentén megoszló (Erők)

### Alapfogalmak

#### Erő

* Def: Testek közötti kölcsönhatás mérete
* Tulajdonságok:
    * Nagyság
    * Hatásvonal
    * Értelme
* Vektormennyiség
* Jel: $F$
* Hatásvonal: az erő vektor meghoszabbítása
* Támadáspont: Az erő és test első közös pontja
* Mértékegysége $[F] = [N] = [\frac{kg*m}{s^2}]$
* 3D-s koordinátarendszerben
* Bázisvektorok: i,j,k, A 3D-s koordinátarendszernek 
* **Jobbkézszabály**

$$
    \begin{bmatrix}
        
    \end{bmatrix}
$$
$$ F = Fx *i + Fy * j + Fz *k$$

* Nem elég 1 vektor, mivel támadáspontot is kell definiálni
* $ra$ helyvektor a jele

### Statika Alapelvei

#### 1. Erőösszeg alapelve

**Azonos támadáspontú erők összegezhetőek**
* Összegzés lehet:
    * Grafikusan (paralelogramma módszer)
    * Vektorosan felírva
* Összegzéssel kapunk egy eredő

#### 2. Két erő egyensúlya alapelv

**Két erő akkor van egyensúlyban hogyha: azonos hatásvonalúak, nagyságúak, de értelmük ellentétes**

$$
    F1 + F2 = 0
$$
$$
    F1 = -F2
$$

#### 3. Egyensúlyi erőrendszer hozzáadása ill. kivonása alapelv

**Egyensúlyi erőrendszer hatása nem változik, ha hozzáadok, vagy elvonok egy önmagával egyensúlyban lévő rendszert**
$$Fe + -F1 + F1 = Fe $$

* **Következmény**: Az erőt a hatásvonal mentén szabadon eltolható

#### 4. Három erő egyensúlya alapelv

* 3 erőnek közös síkban kell lenniük
* Hatásvonalaiknak egy pontban kell metsződniük
* záródó vektorháromszöget alkosson (folytonos nyílfolyammal)
$$ F1+F2+F3=0 $$

#### 5. Akció - Reakció alapelve

**Két testnek egymásra gyakorolt hatása mindig egyenlő egymással, de ellentétes értelmű**

#### 6. Helyettesíthetőség alapelve

**Ha a deformált test deformáció után nyugalomban marad helyettesíthető egy hasonló formáju testel**

### Az erő forgató hatása

* Forgató hatás függ:
    * az erő irányától, nagyságától,
    * Tengelytől mért távolságtól

* M erőtengely körüli forgatónyomaték
* Erőkar az erőtengely és a támadáspont közötti távolság

$$ M = F $$

* $M$-nek iránya is van $\rightarrow$ $M$ is vektormennyiség

$$

M = M*k=

    \begin{bmatrix}
        0 \\
        0 \\
        M
    \end{bmatrix}
$$

* Milyen az iránya?
* **jobkbkézszabály**
* Pozitív ha a kézeden a 

