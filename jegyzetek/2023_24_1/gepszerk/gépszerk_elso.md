# Gépszerk első óra

* Óra előtt felállunk
* Műszaki rajz: szavak nélkül szótérteni más mérnökkel
* Gép méreteit ábrázolja, vagy funkcionalitását
* Vízválasztó tárgy

### Önéletrajz

* hajtáslánc analízis és szim
* Fokozatnélküli hajtóművek
* Pneuatikus vezérlések

### Célkitűzés

* Nemzetközi 2D műszaki rajz szabályainak elsajátítása
* Alapvető szabályokon alapul
* Szabadkézi vázolókézség (gyakorló feladatok szabadkézzel, egyenes)
* Kézi és Gépi modellezés
* Rajzolvasási kézség fejlesztése -> Más műszaki rajzainak olvasása (duh)
* **Mérnöki precizitás**
* Időmenedzsment nagyon fontos!

### Házi feladatok

* Fejes csapszeg műrajz
* 2. Házifeladat: csavarkötés és nyomókötés
* 4. házifeladat: lesz szabadkézi és számítógépes része is (számítósat nyomtatni)

### Általános információk

* Heti kontakórák
* 3 óra előadás (hétfő)
* 2 óra rajztermi gyakorlás
* 5 kredit
* 4 házifeladat
* 2 db ZH
* sok tárgy erre épít át kell menni valahogy
* kapcsolódó anyagok: **Moodle és a tanszéki weboldal**
``www.gt3.bme.hu``
* Moodle hez kell az **edus email**
* Hallgatói regisztráció is szükséges
* Előadásokra megéri bemenni nem minden fólia van a weblapon

### Tárgy honlapjának használata

* Begyakorlókat (házi feladatok melett mappában) a házival kell beadni
* Előadás kivonatok kb fele
* Hallgatói tájékoztató, ütemterv -- Érdemes elolvasni
* Házifeladatoknál vannak előre elkészített lapok
* És feliratmező mostmár kinyomtatható
* DE ki kell tölteni a mezőket magunknak
* Normál fénymásoló papírra nem érdemes -- inkább vastag papírra
* Játszani kell a nyomtatóval

### Házi feladatok bővebben

* Két külön lapra kell
* Szabványírás nagyon finom nagyon jó
* Vonalak a gyakorló alján lehet szabadkézzel
* Betűsablon szar dolog
* Már letölthető
* első házit 4. oktatási héten kell leadni
* 2. feladatnál katalógus használata A3
* 4. feladat már számítógépen egy összeszerelési rajz
* **HETEDIK HÉTIG LEHET BEADNI AZ ELSŐ HÁZIT**
* Három oktatási hét van a beadásra (másodikat 10. hétig lehet a 7. től)
* Először csak átvétel
* Ha elfogadják akkor beveszik de az már kettes
* Ha kifutunk az időből -20% a pontszámból
* a Végén lesz összesítve a pontok jeggyé

### ZH

* Ha ZH val van gáz arra van külön zs kurzus ahol csak azoat kell megírni
* Első zh 9. hét
* második 13. hét
* Pótlás a ZH sávban
* mindegyiket csak egyszer lehet pótolni
* Javításnál új jegy számít (Utolsó az érvényes)
* Ha pótlásról lecsúunk van pót-pót de az díjas
* Számozott helyekre kell leülni
* Ha elmarad sportnak miatt fakultatív pótlás a gyk vezetőnél, nem lesz katalógus

| jegy | pont sáv|
|------|---------|
|1|0-40|
|2|40-55|
|3|55-

### Jegyzet Tanköny, felhasznált irodalom

* Weboldalon minden is fent van
