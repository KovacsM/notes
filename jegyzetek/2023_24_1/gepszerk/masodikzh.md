# Második ZH információk

## Első feladat

### Tűrésszámolás

* Tűrástáblázatban a méretek
* Rjazoljuknk egy alapvonalat
* rá egy testet aminek a két oldalát méretezzük be a tűrések szerint (csap)
* melette egy alapvonalra a másikat is (furat)

#### Legnagyobb játék

* Furat legnagyobb csap, legkisebb furat
* Fedés legkisebb furat, legnagyobb csap

### Szélső értékek számolás

* Max és Min értékeit vesszük a többi méretnek
* Ilesztés jellege: Átmeneti illesztés
## Második feladat

* Előadás fóliái
* Alaktűrés, köralaktűrés, helyzettűrés pozíciótűrés
* A tűrésmező két koncentrikus kör melyek távolsága 0,25 ha belül van jó, ha kívül akkor nem

## Harmadik Feladat

* Sok pont
### Kirészletezés

* Előtte képzeljük el az alkatrészt

### Méretháló

* Mindíg a külső átmérőre rajzoljuk fel a méretvonalat
* Lekerekítések furatok
* ilyenek

### Tűrések

* Ha találkozik két alkatrész h7 ha furat, H7 ha csap
* Forgácsolva, ha forgácsolni lehet
* Általános tűrések: ISO 2768-mK

### Felületi érdesség

* Akkor van teteje a gyökjelnek ha forgácsolva van
* Csak Ra elég nem kell a szám
* Ha alkatrész alkatésszel ábrázoljuk, akkor oda is kell egy tűrés
