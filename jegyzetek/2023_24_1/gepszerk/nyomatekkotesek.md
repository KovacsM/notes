# Nyomatékkötések

## Funkciója

* nyomatékot ad át két gépelem közt

## Osztályozása

### Anyaggal záródó

* Ragasztott
* Ha nincs ráírva semmi hogy (ragasztva...) szilárd illesztés, tengely mérete nagyobb mint a furat mérete, bele van sajtolva, vagy hőmérséklet különbség

#### Kúpos szegkötés

* Apára csücskénél éllekerekítés, végénél élletörés
* Anyának az elején élletörés
* Nagyobb a letörés az anyán mint a lekerekítés az apán ( különben nem fekszik fel )
* felülről átfúrjuk kúppa majd szegecset verünk bele

#### Tangenciális szegkötés

* Oldalról be van ütve egy vékonyodó alkatrész (szeg) ami le van fixálva egy anyával

#### Szorító bilincses kötés

* Rászorítjuk 2 anyával a rúdra a bilincset
* Súrlódási erő viszi át a nyomatékot

#### Lapolásos kötés

* Lelapoljuk a rudunkat, lesz egy egyenes része
* nehéz a hornyot kialakítani az anyában
* Szikraforgácsolás
* Kiönteni
* Marás (de nem mindíg, nehéz lehet kis rádiuszos szerszám
* **Üregeléssel** áthúznak egy szerszámot rajta, ami kimarja az anyagot
* Nem jó nagy nyomatékok átvitelére, nem szimmetrikus, nehéz pontosan megcsinálni

#### Poligonkötés

##### Alap

* Négyzetes a rúd, nagy nyomatékra ez sem jó
* Ha nem egyenes enne az oldala akkor jobb lenne
* Le van törve az éle

##### Kerekített

* Igazi poligonkötésnek le van kerekítve a csücske, és oldali sem egyenesek, nagy sugarú ív
* Sarka ívvel van letörve
* Háromszög és négyszög profil

#### Tengelykötés "Hirth" homlokfokozattal

* Nem kell rajzolni

#### Ékkötések

* Extra alkatrész
* Éket beletoljuk a tárcsába $\rightarrow$ befeszül, de nem lesz szimmetrikus ( ék miatt eltolódik az apa közepe ) nagy fordulatszámnál: ráz, üt

## Alakkal záró

## Tengelyvég kialakítás

## Tengelyirányú rögzítések

## 2. Házi feladat

* Nagyításban a schraffozott alkatrészek méretét változatlanul hagyjuk
* Furat méretét felfúrjuk majd menetet belefúrjuk
* A keréknek a kis cuccai felül vékony vonal
* Begyakorlóban először nagyítást megkeressük
* A-A méretei a példából
* nyomatékkötés méretei
* F1 táblázatból, F2 is
* F3 és F4 képlet alapján (az is táblázatban)
