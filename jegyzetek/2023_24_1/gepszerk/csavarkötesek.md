# Gépszerk 5. előadás

## Csavarmenetek

* Méret jelölés: x utáni szám az egy fordulás alatti $\vec{z}$ irányba való mozgás, (menetemelkedés)
* Whitworth menet: csöveken, angolszász

## Menetek előállítása

* Csavar tövében ha esztergáljuk kifutást kell neki hagyni ( gépet lehessen állítani )
* Ha fúrjuk a menetet, nem lehet teljesen végig fúrni, aljában üres tér (fúrási törmelék helye) , és menetkifutás

### Menetszerszámok

* Kézi menetrú szerszámokon befogás felőli oldalon rovátkák (2 $\rightarrow$ 1 $\rightarrow$ 0 ) a sorrend
* 2 a legdurvább, 0 a legfinomabb, élesebb

### Szabványos kötőelem gyártás

* Nem esztergálják $\rightarrow$ sok forgács keletkezik
* Rúdra menetet tesznek, majd a tetejét rázömítik

## Csavarmenet ábrázolása

* Alul felül a törésvonal a további anyagot jelöli

### Apa

* Végében ahonnan a menetet ráhúzzuk letörést is kellhet ( könnyebben rámenjen az anya)
* Csavarvonalat nem rajzoljuk ki (szerencsére)
* Meg kell adni:
    * Menethatároló vonal: meddig megy a menet (vastag vonal faltól falig)
    * Menet magméretét $\rightarrow$ vékony vonal
    * Oldalnézetből nem teljes vonal jelöli a letörést, a menet magméretét

### Anya

#### Végig van fúrva

* Metszetet rajzolunk
* Furat átmérője az apától függ, de nem a csavar teljes átmérője
* Katalógusból látszik mekkora legyen az első furat pontosan
* Sarkára letörés szükséges, apa könyebben belemenjen
* Menetet utána teszem rá vékony vonallal letöréssel egy "magasnak" kell lennie
* Schraffozást mindig vastag vonalig csináljuk (vényony menet magméret vonalat elhanyagoljuk)
* Majd kihúzzuk a vastag vonalakat

#### Nincs végig fúrva

* Hagyni kell a végén üres teret hogy a fúrásból a törmelék oda menjen
* Furat sem mehet végig ( Menethatároló vonal )
* A menethatároló vonalat tovább kell húzni ( vékony magméret vonalig )
* Oldalnézete olyan mint a másiké ( csak kívül van a magméret vonal )
* Metszetnél megint át schraffozunk a vékony vonalon ( magméret ) a vastag vonalig

---

**A méret mindig kívül van, apán a külső vastag, anyán külső vékony, apára csak azonos méretű anya megy rá!**

* Ha a letörés megegyezik a magmérettel, nem csinálunk semmi extrát
* Ha nagyobb a letörés akkor köré berajzoljuk

### Menetkifutás

* magméret jelző vonala kifele ívelődik a csavarból, nem mindig kell, ha van ez jeleti az eszterga határát

---

**Ha kúpra rajzolunk mindig a felénk lévő élre helyezzük el a menet magméretet jelölő vonalát**

## Csavarkötések

#### Menetes zsákfuratba ráerőssítünk egy alkatrészt, rögzítéshez rugós alátét

1. Összetoljuk az alkatrészeket
2. Apukát köztes alkatrészbe helyezzük (nem kell forgatni köztesen nincsen menet, és nagyobb is mint az apa átmérője)
3. Anyáig letoljuk, és elkezdjük forgatni, **Apa mindig takarja anyát** ( nincsen bevonalkázva a köztes rész)
4. Elérei apa a rugós alátétet, apa fejének elfordulásával is jelezni kellhet
5. Belecsavarjuk a rugós anyában a rugós anya közepén mindig megmarad a kis hézag

#### Anyán van a menet

1. Összetoljuk az alkatrészeket
**Orsómenet megint takarja az anyamenetet**

#### Fontos dolgok

* Anya jó irányba álljon (sarkai, letörések)
* rugós alátét jó irányba dőljön ( tekerem lefele menjen )
* két anyag találkozásánál a vastag vonal megy végig
* Az Apa csavar feje és az anyag találkozásánál is vastag vonal végig megy
* Csavar rajzolás lesz **ZH-ban** is
* ha nincs menetkifutás a határoló vonal átmegy a csavaron és mindenen, ha van csak a csavar határáig ($46/69$ dia)

### Ászok csavar

* ha nem lehet az egészet
* két oldalán a menet nem feltétlen azonos ( bármilyen különbség )
* Becsavarási hossz arányos a csavar hosszával

### Belső csavarkülcsnyílűás

* Sűrűbben lehet őket rakni, nem kell oldalról becsavarni
* Lehet süllyeszteni

### Süllyesztett fejű csavar

* Hasonló mint az előző, de lehet másféle is lehet a fején a cucc

### Csavarbiztosítások
**Fontos mert rázkódások hatására kilazulhatnak, lepöröghetnek**

#### Erővel záró

* Erőt fejt ki az anyára, súrlódás miatt nem mozog ki annyira
* Dupla anyás megoldás ( magasságuknak nem kell ugyanolyannak lennie )
    * Itt is  asurlódás gátol

#### Alakkal záró

* **Sasszeges, Koronás anya:** Rátekerés után átfúrjuk majd sasszeggel lefixáljuk, sasszegnek a végét elhajlítjuk
* **Behajlított tarély, horony** füleket ráhajlítják a horonyra

## 4. Házi feladat

* $55/69$ Diánál kezdődik
* A3-asra
* Házkotő 38. $\rightarrow$ csavarok szerkesztése egyszerűsítetten vagy valami ilyesmi
* Darabjegyzéket nekünk kell kitölteni
* Táblázatból kell megtudni melyik mit kell csinálni
* Schraffozásra nagyon kell figyelni, egy alkatrésznek egy féle a Schraffozása, ha nagyítva van akkor is
* Axonometrikus ábrát nem kell lerajzolni, minden mást igen
* A-A , és B rész nagyítása a lényeg, de a nagy gecit is meg kell rajzolni
* Csavarkötés ábrázolást le lehet másolni a mintából [gt3.bme.hu](gt3.bme.hu)
* **5.B** saját feladatkódnak megfelelő csavarkötést ábrázoljunk a begyakorlón
* begyakorlón kb ugyan annyi a hely, meg lehet nézni 2:1 vagy 5:1 a méretarány
