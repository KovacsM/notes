# Bevezetés

* Elsőnek barlangrajz -> vadászati irányelvek
* Gépkonstrukció szabadkézi vázlat -> 3D ben próbál, pontatlan
* Műszaki rajzok, CAD, modellek -> pontosabb, azonos jelölések
* 4. házinál autoCAD mechanical -> sima autoCADnél több nekünk szóló funkció van benne
* **DIPA** lapra érdemes rajzolni

### Dokumentációk

* Gép termék létrehozásához kell:
  * szöveges rész: szöveges dokumentáció, számolások
  * rajzi rész: műszaki rajz
    * Alak és mérethű, méretarányos
    * Papírra, 2 dimenziós
    * Vetületek
    * _"Sík és tér kétirányú konfliktusa"_

### Rögzítési Technikák

* Kétféle megnevezési rendszer
  * tárgyra utaló ( alkatrészrajz, összeállítási rajz...)
  * Céljára utaló megnevezés

#### Szabadkézi
* Szabadkézi vázlatok
  * Begyakorlók
* Szabadkézi műhelyrajz
  * gyorsan megvan
  * gyors, de pontatlan
* Ceruzával pontosan kiszerkesztett és kihúzott rajzokat
  * Házi feladatok

#### Gépi technikák

* 2Ds és 3Ds -is
* Automatizálható
* CAD-alapjai tárgyban
* _"Térlátás, mérethűség elvesztésének veszélye"_

#### Alkatrészrajz

* Szerelésre kész állapotot ír le
* Teljes mérethálózat
* Felületi érdesség
* Túréshatárok
* Teljes, de csak 1 alkatrész van rajta

#### Összeállítási rajz

* Nincs teljes méretháló
* Precízen kitöltött darabjegyzék
* Külön tűrés, érdesség stb nincs
* Összeszereléshez ad útmutatót

#### Törzsrajz

* Nem szabványos (BMEs találmány)
* kb az Alkatrészrajz és az Összeállítási rajz keveréke
* Kettő egybe van gyúrva

#### Robbantott szerelési rajz

* Szereléshez használt
* Melyik alkatrész hova megy mi megy rá

#### Rajzi követelmények

* rajzlap méret
    * A0-A1-A2-A3-A4
* Rajzterület mérete, rajzlapok összehajtása
    * A jegyzék kerüljön kívülre
    * Szabvány szerinti összehajtás
    * Tételszámos saroknál bontjuk ki egy kézzel kinyitható

#### Feliratmező

* Egyedi kialakítás, a szabvány csak a tartalmukat írja elő
* Betűmagasság is fontos a kitöltésnél
* Weben órai anyagban a pontos méretek

#### Feliratok

* Vonalvastagság mindig a betűmagasságnak a $$\frac{1}{10}$$-e
* Olvasni alulról és jobbról, ha dönteni kell össze vissza nem lehet forgatni
* Méretarány: $$M=\frac{Rajzi méret}{A tárgy valós mérete}$$
* Nagyítani is lehet fordítva mint a kicsinyítés
* Nem ad-hoc csak a megadott méretarányokat lehet használni
* Mindig a valós méretet írjuk rá ($$M=\frac{1}{1}$$)
* Ha lehet akkor természetes nagyságban, torzításmentesen rajzoljunk

#### Tételszámok (Tsz)

* Minden alkatrésznek van
* Jelölési mödszerekből az elsőt használjuk (web)
* Egymás alá célszerű a számokat helyezni (egy oldalra nem úgy mint a pók lába)
* Számozás alulról felfelé növekednek (1 alul max felül)
* Ha nem fér rá a lapra (más lapra kerül) Össze kell őket kapcsolni
* Ilyenkor Fentről lefele növekszik

#### Vonalfajták

* Vastag folytonos: Kontúrvonal
    * Alkatrésznek külső vonalai, lapnak is
* Szaggatott vonal, vékoyn
    * Nem látható éleket jelöl
* Vékony folytonos vonal
    * Satírozás meg ilyenek
* Kétpont-vonal
    * Külső állások jelölése

#### Vonalvastagságok

* Vékony
* Vastag
* Arányuk 2:1 -hez
* Betétből inkább erősebbet de magunkhoz kell mérni
* Vékony is erős csak vékonyabb, ugyan olyan minőség csak méret számít

#### Vetítési módok

* Párhuzaoms merőlegse vetítés ( Ortogonális )
* Párhuzamos ferdeszöges vetítés ( Klingonális )

* Monge-féle két képsíkos ábrázolás
    * Két oldalról nézünk a tárgyra és úgy rajzoljuk le

* Párhuzamos merőleges vetítési módszerek
    * Európai vetítési módszer
    * Szemlélő és koordináta sík között 

* Néha több vetítési sík kell mint a 3
* Kockába becsomagolós ábra (59/62)
* Szükséges és elégséges számú négyzet (hány vetítési sík kell hogy konkrét legyen a tárgy)

#### 1. Házi feladat

* Törzsrajz
* A4-es

* papírt A4, A3
* rotring, radír
* pontos érkezés mert megvernek

## Öszetett testek ábrázolása

* Elemi testekkel való modellezéssel fogunk foglalkozni CAD alapjain elpször

### Csonkolt testek csoportosítása

* Lebontós vagy hozzáadós

#### Áthatási vonal

Két test palástvonalán helyezkedik el

#### Áthatás során a keletkező test lehet
* A testek uniója
* A testek metszete
* Az egyik, vagy másik test kivonása


