# Alapinformációk 2023/24/1

## Informatika és Programozás Alapjai

### Gyak

* Molnár József
* joe@mogi.bme.hu
* 2 ZH:
  * Egyik 20 pont
  * Masik 30 pont
  * 50 pont (25 + 25 ) Vizsga
* www.inflab.bme.hu/~joe
* Zhk saját gépen

### Eloadas

* 7,8,9. héten elso ZH
* 14 heten masodik zh
* Vizsga szobeli kerdes (2 db)

## Ergonómia

* www.erg.bme.hu
* edu.gtk.bme.hu
* félévközi jegy
* Hercegfi Károly
* 3 gyakorlaton kell bent lenni
* május 13-17 online ZH (12-12) kérdés (negyed óra) (felülírja az óraközi zh-t) (feleletválasztós, egyéb típusok, nem esszé kicsit bonyolultabb)
* Vagy 3 oraközi teszt ( 2.26, 4.15., 05.13.) (6-6 kérdés) (feleletválasztós)
* 2 kis hf május 5. 12:00, május 26, 23:59 (online adod be moodle) (egy oldal, ergonómiai probléma képpel illusztrálva)
* letölthető sablon pdfben elmentve moodle feltoltes
* Póthéten pótzh papíron jelenléti (kérdések hasonlóak)
* Utso óra Szoftver-ergonómia

## Szilárdságtan

* Vizsgás
HF1 04/08
HF2 05/13
ZH 04/25 Cs 18:00-20:00

## Sziltan Gyak

* M3
* Fodor Gergő
* gergo.fodor@mm.bme.hu
* Feladatok megoldása példatárból

