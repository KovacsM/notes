# Sziltan első óra

* Statika merev testek
* Sziltanban már deformálódhat a test
* Cél a deformációk és feszültségek számítása
* **Lineárisan** rugalmas anyagi viselkedés kis deformációk melett
* Beam és Truss (Gerenda és rúd)

# Rudak gerendák állapota (Normál igénybevétel)

## Rudak gerendák feszültségi és alakváltozási állapota

## 1.1 Normál igénybevétel

* Ideális húzókísérletek

* Nyakasodás képlékeny deformációval nem foglalkozunk
$$\Delta L=L-L_0 \ [m]$$
$$\Delta H=H-H_0 \ [m]$$
$$\Delta B=B-B_0 \ [m]$$

### Fajlagos megnyúlás

* Fajlagos megnyúlás (mérnöki alakváltozás): $\epsilon = \frac{\Delta L}{L_0}$

$$-1 \lt \epsilon$$

* Keresztirányban

$$ \epsilon_k = \frac{\Delta H}{H_0} $$
$$ \epsilon_k = \frac{\Delta B}{B_0} $$

* Izotrop (irányfüggetlen) anyag viselkedés esetén
* $\epsilon$ angolul: **engeneering strain**
* Elemi sziltanban $\epsilon \lt 1,05$

* Logaritmikus strainnel pontosabb, de kis deformációknál elhanyagolható a különbség

### Feszültség

* Mechanikai normál feszültség (adott felületre merőleges): $\sigma$
* Keresztmetszet mentén egyenletesen oszlik el (állandó)
$$Feszültség = \frac{Force}{Area}$$

$$1MPa = 10^6Pa=10^6\frac{N}{m^2}=1\frac{N}{mm^2}$$

* Ha $\epsilon$ kicsi akkor $a = a$

---

* Acél húzó diagram (x: $\epsilon$, y:$\sigma$) (szakítószilárdságlabor)
* $\sigma_F$ kezdeti folyáshatár
* $R_m$ Szakítószilárdság

### Egyszerű Hooke-törvény

$$\sigma = E*\epsilon$$
* Rugalmassági modulusz: $E \ [Pa]$
* Young's Modulus, elastic modulus
* $E \approx 210\ GPa$

---

$$\Delta L = \frac{F* L_0}{A_0*E}$$

* Anyagot homogénnek kezeljük (anyagtulajdonságok helyfüggetlenek)
* Keresztirányú deformációk
$$ \frac{\epsilon_k}{\epsilon} = -\nu$$
$$\nu\ [1]$$
* Poisson-tényező
* $-1>\nu>\frac{1}{2}$
* Ha 0,5 akkor teljesen összenyomhatatlan
* acéloknál $\nu = 0.3$
* Az egyszerű hooke-torvény az alábbi két összefüggést jelenti: (1.,2.)

1. $$\sigma = E*\epsilon$$
2. $$ \frac{\epsilon_k}{\epsilon} = -\nu$$

---

### Méretezés ellenőrzés

$$\sigma_{megengedett}=\frac{\sigma_F}{n}$$
* n $\rightarrow$ biztonsági tényező
$$\sigma = \frac{F}{A_{megengedett}}=\sigma_{megengedett} \Rightarrow A_{megengedett} = \frac{F}{\sigma_{megengedett}}$$

---

* Diák végén extra érdekességek otthon megéri átnézni
